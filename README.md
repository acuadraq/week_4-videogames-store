# Week_4 Videogames Store

![](public/Screenshot.png)

## Description

This is the weekly assignment for the fourth week. Its a videogames store. This are the main points of the page:

- All of the games are retrieve from an json api server.
- On the main page we have a list of the games.
- Every game has a detail page with their comments.
- The home page has a pagination by 8 games per page.

If you want to tested in real time use this url: https://week-4-videogames-store.vercel.app/
