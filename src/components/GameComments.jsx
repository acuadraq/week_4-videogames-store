import React from 'react';
import user from '../user.png';

export const GameComments = ({ comments }) => {
  return (
    <div className="comments__container">
      <h2>Game Reviews</h2>
      <hr />
      {comments.map(comment => (
        <div key={comment.id} className="comment">
          <div className="comment__header">
            <span>
              <img src={user} width="32" height="32" alt="User Icon" />
            </span>
            <span className="comment__user">{comment.user}</span>
          </div>
          <hr />
          <p>{comment.comment}</p>
        </div>
      ))}
    </div>
  );
};
