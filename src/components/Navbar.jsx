import React from 'react';

export const Navbar = ({ changePage }) => {
  return (
    <header>
      <nav className="flex-class">
        <ul className="flex-class">
          <li onClick={() => changePage(0)} className="navbar__title">
            Applaudo Buy
          </li>
          <div>
            <li onClick={() => changePage(0)}>Home</li>
          </div>
        </ul>
      </nav>
    </header>
  );
};
