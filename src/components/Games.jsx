import React from 'react';

export const Games = ({ games, handleDetail }) => {
  return (
    <>
      {games.map(game => (
        <div
          className="game__card"
          key={game.id}
          onClick={() => handleDetail(game.id)}
        >
          <img src={game.image} alt={game.title} width="320" height="400" />
          <div className="game__content">
            <h3>{game.title}</h3>
            <div className="game__footer flex-class">
              <button href="#">Buy</button>
              <span>${game.price}</span>
            </div>
          </div>
        </div>
      ))}
    </>
  );
};
