import React from 'react';

export const Pagination = ({
  gamesPerPage,
  totalGames,
  paginate,
  currentPage,
}) => {
  const pageNumbers = [];
  for (let i = 1; i <= Math.ceil(totalGames / gamesPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <ul className="pagination">
      {pageNumbers.map(number => (
        <div key={number} onClick={() => paginate(number)}>
          {currentPage === number ? (
            <li className="currentPage">{number}</li>
          ) : (
            <li key={number}>{number}</li>
          )}
        </div>
      ))}
    </ul>
  );
};
