import React, { useState, useEffect } from 'react';
import { getGames, getComments } from '../adapters/Fetch';
import { GameComments } from './GameComments';
import shop from '../shop.png';

export const GameDetail = ({ id }) => {
  const [gameInfo, setGameInfo] = useState({});
  const [comments, setComments] = useState([]);
  const [loading, setLoading] = useState(false);

  const getGameInfo = async () => {
    setLoading(true);
    const gameInfoApi = await getGames(id);
    setGameInfo(gameInfoApi);
    const commentsApi = await getComments(id);
    setComments(commentsApi);
    setLoading(false);
  };

  useEffect(() => {
    getGameInfo();
  }, []);

  return (
    <>
      {loading && <div className="animation__loading"></div>}
      {!loading && (
        <>
          <div className="game-detail__container">
            <div>
              <img
                src={gameInfo.image}
                alt={gameInfo.title}
                width="380"
                height="500"
                className="game__image"
              />
            </div>
            <div className="game__content">
              <h2>{gameInfo.title}</h2>
              <p>{gameInfo.info}</p>
              <div className="game__footer">
                <p className="game__price">${gameInfo.price}</p>
                <button>
                  <img src={shop} alt="Shopping Cart" width="24" height="24" />
                  <span>Add to Cart</span>
                </button>
              </div>
            </div>
          </div>
          <GameComments comments={comments} />
        </>
      )}
    </>
  );
};
