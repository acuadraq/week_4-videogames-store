import React, { useState, useEffect } from 'react';
import { getGames } from '../adapters/Fetch';
import { Pagination } from './Pagination';
import { Games } from './Games';

export const GamesList = ({ changePage }) => {
  const [games, setGames] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [gamesPerPage] = useState(8);

  const getList = async () => {
    setLoading(true);
    const list = await getGames();
    setGames(list);
    setLoading(false);
  };

  useEffect(() => {
    getList();
  }, []);

  const indexOfLastGame = currentPage * gamesPerPage;
  const indexOfFirstGame = indexOfLastGame - gamesPerPage;
  const currentGames = games.slice(indexOfFirstGame, indexOfLastGame);

  const paginate = pageNumber => setCurrentPage(pageNumber);

  const handleDetail = id => changePage(id);

  return (
    <>
      <h2>GAMES AVAILABLE</h2>
      <hr />
      {loading && <div className="animation__loading"></div>}
      {!loading && (
        <>
          <div className="games__container">
            <Games games={currentGames} handleDetail={handleDetail} />
          </div>
          <Pagination
            gamesPerPage={gamesPerPage}
            totalGames={games.length}
            paginate={paginate}
            currentPage={currentPage}
          />
        </>
      )}
    </>
  );
};
