const fetchApi = async extension => {
  const response = await fetch(
    `https://json-project-posts.herokuapp.com${extension}`
  );
  return await response.json();
};

export const getGames = id => fetchApi(`/games/${id ? id : ''}`);

export const getComments = id => fetchApi(`/games/${id}/comments`);
