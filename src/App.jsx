import React, { useState } from 'react';
import { Navbar } from './components/Navbar';
import { Footer } from './components/Footer';
import { GamesList } from './components/GamesList';
import { GameDetail } from './components/GameDetail';
import './styles/styles.scss';

export const App = () => {
  const [page, setPage] = useState(0);

  const changePage = id => setPage(id);

  return (
    <>
      <Navbar changePage={changePage} />
      <section>
        <div className="container">
          {page === 0 && <GamesList changePage={changePage} />}
          {page > 0 && <GameDetail id={page} />}
        </div>
      </section>
      <Footer changePage={changePage} />
    </>
  );
};
